import java.util.Scanner;

public class LoopDemo10 {
	public void printOdd(int num) {
		for (int i = 1; i <= num; i++) {
			System.out.print(i*2+ " ");
		}
	}

	public void printEven(int num) {
		for (int i = 2; i <= num; i++) {
			System.out.print(i*2+ " ");
		}
	}

	public void printFull(int num) {
		for (int i = 0; i <= num; i++) {
			System.out.print(i+ " ");
		}
	}

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter the number : ");

		int num = scan.nextInt();
		System.out.println("Ente the operation\n" 
		+ "1=odd \n" 
		+ "2=Even \n"
		+ "3=Full \n");
		
		String operation=scan.next();
		LoopDemo10 Math= new LoopDemo10();
		
		switch (operation) {
		case "1": Math.printOdd(num);
			break;
		case "2" : Math.printEven(num);
			break;
		case "3": Math.printFull(num);
			break;
		}
		
		

	}
}
